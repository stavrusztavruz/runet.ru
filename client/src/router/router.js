import Vue from 'vue'
import Router from 'vue-router'


Vue.use(Router);

const router = new Router({
    mode: 'history',
    routes: [

        {
            path: '/',
            name: 'lending',
            props: true,
            meta: {layout: 'main'},
            component: () => import('../views/user/lending'),
        },
        // {
        //     path: '/main',
        //     name: 'main',
        //     props: true,
        //     meta: {layout: 'main'},
        //     component: () => import('../views/user/1_main/main'),
        // },
        {
            path: '/recommendation',
            name: 'recommendation',
            props: true,
            meta: {layout: 'main'},
            component: () => import('../views/user/2_recommendation/recommendation'),
        },
        {
            path: '/forwhom',
            name: 'forwhom',
            props: true,
            meta: {layout: 'main'},
            component: () => import('../views/user/3_forwhom/forwhom'),
        },
        {
            path: '/advantages',
            name: 'advantages',
            props: true,
            meta: {layout: 'main'},
            component: () => import('../views/user/4_advantages/advantages'),
        },
        {
            path: '/reviews',
            name: 'reviews',
            props: true,
            meta: {layout: 'main'},
            component: () => import('../views/user/5_reviews/reviews'),
        },
        {
            path: '/complectation',
            name: 'complectation',
            props: true,
            meta: {layout: 'main'},
            component: () => import('../views/user/6_complectation/complectation'),
        },
        {
            path: '/tworecommendation',
            name: 'tworecommendation',
            props: true,
            meta: {layout: 'main'},
            component: () => import('../views/user/7_recommendation/tworecommendation'),
        },
        {
            path: '/howtoget',
            name: 'howtoget',
            props: true,
            meta: {layout: 'main'},
            component: () => import('../views/user/8_howtoget/howtoget'),
        },
        {
            path: '/answers',
            name: 'answers',
            props: true,
            meta: {layout: 'main'},
            component: () => import('../views/user/9_answers/answers'),
        },


        // // ========ERROR===========
        // {
        //     path: '*',
        //     name: '404',
        //     meta: {layout: 'empty', auth: true},
        //     component: () => import('../views/404'),
        // },
    ]
})

// router.beforeEach((to,from,next)=>{
//     const requireAuth = to.matched.some(rout => rout.meta.auth);
//     const currentUser = localStorage.getItem("authUser");
//
//     const fromRout = from.name;
//     const toRout = to.name;
//
//     if(requireAuth && currentUser == null)
//     {
//         // console.log("Не авторизирован");
//         next({name: "login"});
//
//     }else{
//         // console.log("Авторизован");
//         next();
//     }
//
// })

export default router;