import axios from "axios";
import keys from '@/../configs/keys'

export default {
    state:
        {
            users: [],


        },
    actions:
        {

            async SEND_CALLBACK(state, formData) {
                // let token = await localStorage.getItem('token');
                let url = `${keys.mailHost}/runetgetcallback`;

                return axios({
                    method: "POST",
                    url: url,
                    data: formData,

                }).then((response) => {
                    console.log(response.request);
                    return response.request;
                }).catch((error) => {
                    console.log(error);
                    return error.request;
                })
            },

        },
    mutations:
        {


        },
    getters:
        {

        }
}