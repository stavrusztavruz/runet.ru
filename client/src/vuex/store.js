import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import keys from '../../configs/keys'

import message           from './user/message';




Vue.use(Vuex);

let store = new Vuex.Store({
    state: {

    },
    mutations: {

    },
    actions: {


    },
    getters: {
    },
    modules:{
        message,

    }

});

export default store;