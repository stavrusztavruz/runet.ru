const express = require("express");
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const mongoose = require('mongoose');
const passport = require('passport');

const transportsRoutes = require('./routes/user/transportsRoutes');
const usersRoutes = require('./routes/user/usersRoutes');


// Подключение к БД
const keys = require('./config/keys-api');

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);
mongoose.connect(keys.MongoDB_URI, {useNewUrlParser: true})

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    // we're connected!
    console.log(`MongoDB connected!`);
});

app.use(passport.initialize());
require('./middleware/passport')(passport);

app.use(morgan('dev'));
app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use('/api/phone',       transportsRoutes);
app.use('/api/users',            usersRoutes);

module.exports = app;